import streamlit as st
import sqlite3
import base64
import pandas as pd
import numpy as np
from keras.models import model_from_json
from keras import backend as K
from firebase import firebase 
import matplotlib.pyplot as plt
from scipy.ndimage.filters import uniform_filter1d, gaussian_filter
import mtcnn
import keras
from keras_contrib.layers.normalization.instancenormalization import InstanceNormalization
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.models import load_model
import random
import sqlite3

firebase = firebase.FirebaseApplication('https://collage-project-25c75-default-rtdb.firebaseio.com', None)
id1 = firebase.get('/id/token/', '')
query_params = st.experimental_get_query_params()

import cv2
from PIL import Image
import os
import tensorflow as tf
from numpy import asarray
from numpy import array
from mtcnn.mtcnn import MTCNN
from numpy import expand_dims
from numpy import reshape
from numpy import load
from numpy import max
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import Normalizer
from sklearn.svm import SVC
from Res_Model import create_model
conn = sqlite3.connect('details.db')
c= conn.cursor() 
def create_table():    
    c.execute('CREATE TABLE IF NOT EXISTS detail(name TEXT UNIQUE,city TEXT NOT NULL,age TEXT NOT NULL)')
def insert(name,city,age):
    conn=sqlite3.connect('details.db')
    cur = conn.cursor() 
    cur.execute('INSERT INTO detail VALUES(?,?,?)', (name, city,age))
    st.success('Register Entry sucess')
    conn.commit()
    conn.close() 

def view_all_users():
    c.execute('SELECT * FROM detail')
    data = c.fetchall()
    return data 
def search(name):
    c.execute('SELECT * FROM detail WHERE name =?',(name,))
    data = c.fetchall() 
    return data
def distance(emb1, emb2):
    return np.sum(np.square(emb1 - emb2))

def draw_facebox(result_list,image_1):
  x, y, width, height = result_list[0]['box']
  img_res = image_1.crop((x, y, x + 185 ,y + 185))
  return img_res

def extract_image(image):
  img1 = Image.open(image)
  img1 = img1.convert('RGB')
  pixels = asarray(img1)
  detector = MTCNN()
  f = detector.detect_faces(pixels)
  x1,y1,w,h = f[0]['box']
  x1, y1 = abs(x1), abs(y1)
  x2 = abs(x1+w)
  y2 = abs(y1+h)
  store_face = pixels[y1:y2,x1:x2]
  image1 = Image.fromarray(store_face,'RGB')
  image1 = image1.resize((185,185))
  face_array = asarray(image1)
  return face_array
#extracting embeddings
def extract_embeddings(model,face_pixels):
  face_pixels = face_pixels.astype('float32')
  mean = face_pixels.mean()
  std  = face_pixels.std()
  face_pixels = (face_pixels - mean)/std
  print(face_pixels.shape)
  samples = expand_dims(face_pixels,axis=0)
  
  yhat = model.predict(samples)
  return yhat[0]

def main():
    st.set_page_config(page_title="Forensic helper",
                initial_sidebar_state="collapsed",
                page_icon="🔮")
    
    menu = ["Home","Login"]
    choice = st.sidebar.selectbox("Menu",menu)
    
    if choice == "Home":
        
        st.text(" ")
        st.text(" ")
        st.text(" ")
        st.text(" ")         
        st.text(" ")
        st.text(" ")
        st.text(" ")
        st.text(" ")
        
        html_temp = """ 
            <div style="border:5px black;background-color: #000000;border-style: outset;">  
            <h1 style ="color:#FFFF00;
                        text-align:center;
                        font-weight:bold;
                        font-size:85px;
                        font-style:normal;
                        font-family:"Times New Roman", Times, serif;">FORENSIC ASSISTANCE SOFTWARE</h1>   </div>
            """
            
        st.markdown(html_temp, unsafe_allow_html=True) 
        st.snow()
    elif choice == "Login":
        
            if id1 == query_params["token"][0]:
                print(query_params)
                print(id1)
                list_of_pages = ['create sketch','Photorealistic face generation','Face completion','Face matching']
                page= st.radio("TASKS",list_of_pages)
                #if page =="Insert details":
                #    create_table()
                 #   name = st.text_input("Enter the NAME","")
                 #   city = st.text_input("Enter the city","")
                 #   age = st.text_input("Enter the age","")
                 #   if st.button("Enter details"):
                 #       insert(name,city,age)
                 #   if st.button('view DB'):
                 #       data1 = view_all_users()
                 #       st.write(data1)
                if page == "create sketch":
                    
                    html_temp = """ 
                        <div> 
                        <h1 style ="color:black;
                                    text-align:center;
                                    font-weight:bold;
                                    font-size:40px;
                                    font-style:normal;
                                    font-family:Courier New;">'CompoSketch feature'</h1>
                                    <h2>This feature allows you to draw a sketch or drag and drop facial features from your system to create a face</h2>
                                    <div id='buttons' class="button1" >
                        <a href="https://canvastoolkit.vercel.app/" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary navbar-btn btn-shadow btn-gradient">Click to create sketch</a>
                    </div> 
                                    </div>
                        """
                        
                    st.markdown(html_temp, unsafe_allow_html=True)                
                if page=='Photorealistic face generation':
                    html_temp = """ 
                        <div> 
                        <h1 style ="color:black;
                                    text-align:center;
                                    font-weight:bold;
                                    font-size:50px;
                                    font-style:normal;
                                    font-family:"Lucida Console", "Courier New", monospace;">Sketch to photo generation</h1>  
                        </div> 
                        """
                    st.markdown(html_temp, unsafe_allow_html=True)
                    with st.spinner('Loading Model Into Memory....'):
                            g_model = load_model('g_model.h5',custom_objects={'InstanceNormalization':InstanceNormalization})
                        
                    sketch_file = st.file_uploader(
                        label="Enter the sketch", type=['jpg'])
                    

                    if sketch_file is not None:
                        image = Image.open(sketch_file)
                        
                        img = img_to_array(image)
                        dim = (256, 256)
  
                        # resize image
                        im_cv = cv2.resize(img, dim, interpolation = cv2.INTER_NEAREST)
                        im_rgb = cv2.cvtColor(im_cv, cv2.COLOR_BGR2RGB)
                        # convert to numpy array
                        img = img_to_array(im_rgb)
                        
                        
                        norm_img = (img.copy() - 127.5) / 127.5

                        g_img = g_model.predict(np.expand_dims(norm_img, 0))[0]
                        g_img = g_img * 127.5 + 127.5
                        
                        col1, col2 = st.columns(2)
                        with col1:
                            st.image(image, caption='Sketch to be tested')
                        with col2:
                            
                            g_img=cv2.cvtColor(g_img, cv2.COLOR_BGR2RGB)
                            cv2.imwrite('temporary.jpg', g_img)
                            st.image('temporary.jpg',clamp=True, channels='RGB',caption='Generated Face')
                            
                if page =='Face completion':
                    
                    
                    html_temp = """ 
                        <div> 
                        <h1 style ="color:black;
                                    text-align:center;
                                    font-weight:bold;
                                    font-size:40px;
                                    font-style:normal;
                                    font-family:Courier New;">'Face Completion'</h1>
                                    <h2>This feature takes in a masked image and generates a completed face</h2>
                                    <div id='buttons' class="button1" >
                        <a href="https://share.streamlit.io/cicykagnes/clg-project/main/app.py" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary navbar-btn btn-shadow btn-gradient">Click to go to the page</a>
                    </div> 
                                    </div>
                        """
                        
                    st.markdown(html_temp, unsafe_allow_html=True)
                    
                if page =='Face matching':
                    st.header('FACE-MATCHING')
                    match_file = st.file_uploader(
                        label="Enter the photo to be matched", type=['jpg'])
                    if match_file is not None:
                        face = extract_image(match_file)
                        testx = asarray(face)
                        testx = testx.reshape(-1,185,185,3)
                        model = create_model()
                        model.load_weights("model.h5") 
                        new_testx = list()
                        for test_pixels in testx:
                            embeddings = extract_embeddings(model,test_pixels)
                            new_testx.append(embeddings)
                        new_testx = asarray(new_testx)  
                        data1 = load('Face_collection_compressed_new.npz')
                        train_x,train_y = data1['arr_0'],data1['arr_1']

                        data = load('Face_collection_embeddings.npz')
                        trainx,trainy= data['arr_0'],data['arr_1']
                        #normalize the input data
                        in_encode = Normalizer(norm='l2')
                        trainx = in_encode.transform(trainx)
                        new_testx = in_encode.transform(new_testx)

                        #create a label vector
                        new_testy = trainy 
                        out_encode = LabelEncoder()
                        out_encode.fit(trainy)
                        trainy = out_encode.transform(trainy)
                        new_testy = out_encode.transform(new_testy)

                        #define svm classifier model 
                        modelsvc =SVC(kernel='linear', probability=True)
                        modelsvc.fit(trainx,trainy)

                        #predict
                        predict_train = modelsvc.predict(trainx)
                        predict_test = modelsvc.predict(new_testx)

                        #get the confidence score
                        probability = modelsvc.predict_proba(new_testx)
                        confidence = max(probability)

                        #Accuracy
                        acc_train = accuracy_score(trainy,predict_train)
                        trainy_list = list(trainy)
                        p=int(predict_test)
                        if p in trainy_list:
                            val = trainy_list.index(p)
                        colu1,colu2 = st.columns(2)
                        with colu1:
                            st.subheader('Input photo')
                            st.image(face)
                        with colu2:
                            st.subheader('Found match')
                            st.image(train_x[val])
                        st.balloons()
                        
                        trainy = out_encode.inverse_transform(trainy)
                                 
                        
                        st.subheader('Details of the match')
                        
                        db_result=search(trainy[val])
                        a,b,c = db_result[0]
                        c1,c2,c3,c4 = st.columns(4)

                        with c1:
                            st.write('Name')
                            st.write('City')
                            st.write('Age')
                        with c2:
                            st.text(a)
                            st.text(b) 
                            st.text(c)
            else:
                 st.warning("Invalid Token Id")
                 print(query_params["token"][0])
                 print(id1)

   
if __name__=='__main__':
    
    main()
